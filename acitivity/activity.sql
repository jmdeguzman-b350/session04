/*
Create an solution.sql file inside s04-activity folder and do the following using the music_db database:
*/

/*
    1. Find all artists that has letter d in its name.
*/
SELECT * FROM artist WHERE full_name LIKE "%d%";

/*
    2. Find all songs that has a length of less than 4 minutes.
*/
SELECT * FROM songs WHERE length < 400;

/*
 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
*/

SELECT album_title, song_name, length FROM artist
    JOIN albums ON artist.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id;
/*
4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
*/

SELECT * FROM artist
    JOIN albums ON artist.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id
    WHERE album_title LIKE "%a%";


/*
 5. Sort the albums in Z-A order. (Show only the first 4 records.)
*/

SELECT * FROM songs ORDER BY song_name DESC LIMIT 4;
/*
6. Join the 'albums' and 'songs' tables and find all songs greater than 3 minutes 30 seconds. (Sort albums from descending order)
*/
SELECT * FROM artist
    JOIN albums ON artist.id = albums.artist_id
    JOIN songs ON albums.id = songs.album_id
    WHERE length >= 330;